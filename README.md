mruby
=====

This repository is obsolete.

Visit the repository of [monami-ya.mrb](https://bitbucket.org/mruby/monami-ya.mrb/). You'll find some extensions for embeddians to mruby.

Or visit [the mother repository of mruby on github.com](http://github.com/mruby/mruby/)
